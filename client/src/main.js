/* 
   axios  用于ajax请求
   pinia
   sass 
   vue-router 官网查找安装的版本  npm install vue-router@4
   naive-ui 官网看安装命令，除了ui还有字体  ui框架 npm i -D naive-ui     字体 npm i -D vfonts
   wangeditor  官网vue3安装方式  npm install @wangeditor/editor-for-vue@next --save
*/
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import naive from 'naive-ui' //引入ui框架,提示信息组件
import { createDiscreteApi } from 'naive-ui' //引入ui框架,提示信息组件
import { createPinia } from "pinia"
import { router } from "./common/router"
import axios from 'axios'
import { AdminStore } from "./stores/AdminStore" //引入保存token的文件
//服务端请求地址  相当于设置全局地址，固定请求这个端口的服务器
axios.defaults.baseURL = "http://localhost:7766"

const { message, notification, dialog } = createDiscreteApi(["message", "dialog", "notification"]) //声明提示信息组件


const app = createApp(App)
//将axios全局提供，放入"axios"，可以全局调用 provide
app.provide("axios", axios)

//ui框架的信息提示组件，可以全局使用
app.provide("message", message)
app.provide("dialog", dialog)
app.provide("notification", notification)
app.provide("server_url", axios.defaults.baseURL)


app.use(naive) //ui框架的使用

app.use(createPinia())
app.use(router)

//拦截器，每一个请求都会在header上面添加token。token来自于保存起来的adminStore文件里
const adminStore = AdminStore()
axios.interceptors.request.use((config) => {
   config.headers.token = adminStore.token
   return config
})



app.mount('#app')
