//路由页面
import { createRouter, createWebHashHistory } from "vue-router"

let routes = [
  { path: "/test", component: () => import("../views/Test.vue") },//测试页面的路由
  { path: "/login", component: () => import("../views/Login.vue") },//登录页面的路由
  { path: "/", component: () => import("../views/HomePage.vue") },//首页的路由
  { path: "/detail", component: () => import("../views/Detail.vue") },//文章详情页的路由

  {//后台页面的路由,包含两个二级路由
    path: "/dashboard", component: () => import("../views/dashboard/Dashboard.vue"), children: [
      { path: "/dashboard/category", component: () => import("../views/dashboard/Category.vue") },
      { path: "/dashboard/article", component: () => import("../views/dashboard/Article.vue") },
    ]
  },
]


const router = createRouter({
  history: createWebHashHistory(),
  routes,

})


export { router, routes }