//数据库的搭建

const sqlite3 = require('sqlite3').verbose() //引入数据库
const path = require("path")

const GenId = require("../utils/SnowFlake") //引入雪花id

var db = new sqlite3.Database(path.join(__dirname, "blog.sqlite3"))//连接数据库
const genid = new GenId({ WorkerId: 1 })

//由于db可能要连续调用回调函数的返回值，进行嵌套查询，可能会形成回调地狱，所以对数据库进行promise封装

db.async = {}

db.async.all = (sql, params) => {//把异步变为同步，等待处理完成
  return new Promise((resolve, reject) => {
    db.all(sql, params, (err, rows) => {//执行sql语句，传入参数
      resolve({ err, rows })//查完数据库得到的值
    })
  })
}

//修改和添加
db.async.run = (sql, params) => {
  return new Promise((resolve, reject) => {
    db.run(sql, params, (err, rows) => {
      resolve({ err, rows })
    })
  })
}

//导出
module.exports = { db, genid }
