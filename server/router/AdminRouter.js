//管理员路由

const express = require("express")
const Router = express.Router()
const { db, genid } = require("../db/DBUtils")
const { v4: uuidv4 } = require("uuid")//node uuid 用来生成一个id

Router.post("/login", async (req, res) => {

  let { account, password } = req.body
  let { err, rows } = await db.async.all("select * from `admin` where `account` = ? AND `password` = ?", [account, password])

  if (err == null && rows.length > 0) {//当没有错误结果，表示存在数据，且返回的数据length > 0时 
    //console.log(rows)
    let login_token = uuidv4()//使用uuid生成token
    let update_token_sql = "UPDATE `admin` SET `token` = ? where `id`=?" //根据在数据库查询所得的id，将token保存
    await db.async.run(update_token_sql, [login_token, rows[0].id])//rows[0].id 即为查询数据库所得id

    //将token返回给前端
    let admin_info = rows[0]
    admin_info.token = login_token
    admin_info.password = "" //处理密码，只将用户id，账户，token返回，不能暴露密码

    res.send({
      code: 200,
      msg: '登陆成功',
      data: admin_info//将token返回给前端
    })
  } else {
    res.send({
      code: 500,
      msg: '登陆失败'
    })
  }
})





//导出路由
module.exports = Router