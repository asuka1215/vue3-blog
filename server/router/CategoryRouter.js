//分类路由

const express = require("express")
const Router = express.Router()
const { db, genid } = require("../db/DBUtils")

//列表接口
Router.get("/list", async (req, res) => {

  const search_sql = "SELECT * FROM  `category`"
  let { err, rows } = await db.async.all(search_sql, [])

  if (err == null) {
    res.send({
      code: 200,
      msg: '查询成功',
      rows //对象的简易书写
    })
  } else {
    res.send({
      code: 500,
      msg: '查询失败'
    })
  }
})


//添加接口  /category/_token/add
Router.post("/_token/add", async (req, res) => {
  let { name } = req.body
  const insert_sql = "INSERT INTO `category` (`id`,`name`) VALUES (?,?)"
  let { err, rows } = await db.async.run(insert_sql, [genid.NextId(), name])
  if (err == null) {
    res.send({
      code: 200,
      msg: '添加成功'
    })
  } else {
    res.send({
      code: 500,
      msg: '添加失败'
    })
  }
})


//修改接口  /category/_token/update
Router.put("/_token/update", async (req, res) => {
  let { id, name } = req.body
  const update_sql = "UPDATE `category` SET `name`= ? WHERE `id`= ?"//修改的sql语句
  let { err, rows } = await db.async.run(update_sql, [name, id])
  if (err == null) {
    res.send({
      code: 200,
      msg: '修改成功'
    })
  } else {
    res.send({
      code: 500,
      msg: '修改失败'
    })
  }
})

//删除接口 /category/_token/delete?id=xxx
Router.delete("/_token/delete", async (req, res) => {

  let id = req.query.id
  const delete_sql = "DELETE FROM `category`  WHERE `id`= ?"//修改的sql语句
  let { err, rows } = await db.async.run(delete_sql, [id])
  if (err == null) {
    res.send({
      code: 200,
      msg: '删除成功'
    })
  } else {
    res.send({
      code: 500,
      msg: '删除失败'
    })
  }
})



//导出路由
module.exports = Router



/* 

//前端登录后，服务器端会把token传回前端，其那段需要修改内容时，必须要在header中携带token进行验证后才能修改内容
  let { token } = req.headers
  //console.log(token) //token是唯一的，如果传过来的token和admin表中匹配，那么就证明已经登录

  let admin_token_sql = " SELECT * FROM `admin` WHERE `token` = ? " //在admin表中查询token
  //查询结果
  let adminResult = await db.async.all(admin_token_sql, [token]) // [token]为参数，即传入的token值在admin表中进行匹配
  if (adminResult.err != null || adminResult.rows.length == 0) {
    //如果查询的结果 错误不等于空 或者 长度等于0，说明用户没有登录
    res.send({
      code: 403,
      msg: '请先登录'
    })
  }

*/