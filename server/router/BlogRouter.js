//博客表的增删改查
const express = require("express")
const Router = express.Router()
const { db, genid } = require("../db/DBUtils")


//添加博客  /blog/_token/add
Router.post("/_token/add", async (req, res) => {

  let { title, categoryId, content } = req.body //将标题，分类id，内容当成参数，分类id与category表中的id对应，确定添加在这个分类下面
  let id = genid.NextId()//blog的随机id
  let create_time = new Date().getTime() //blog的时间戳

  const insert_sql = "INSERT INTO `blog` (`id`,`title`,`category_id`,`content`,`create_time`) VALUES (?,?,?,?,?)"//写入blog的sql语句
  let params = [id, title, categoryId, content, create_time]//参数

  let { err, rows } = await db.async.run(insert_sql, params)

  if (err == null) {
    res.send({
      code: 200,
      msg: '添加成功',
      // rows //对象的简易书写
    })
  } else {
    res.send({
      code: 500,
      msg: '添加失败'
    })
  }

})


//修改博客  /blog/_token/update
Router.put("/_token/update", async (req, res) => {
  let { id, title, categoryId, content } = req.body
  let create_time = new Date().getTime() //blog的时间戳
  const update_sql = "UPDATE `blog` SET `title` = ?,`content` = ?,`category_id` = ? WHERE `id` = ?"//写入blog的sql语句
  let params = [title, content, categoryId, id]//参数
  let { err, rows } = await db.async.run(update_sql, params)
  if (err == null) {
    res.send({
      code: 200,
      msg: '修改成功',
      // rows //对象的简易书写
    })
  } else {
    res.send({
      code: 500,
      msg: '修改失败'
    })
  }
})

//删除blog /blog/_token/delete?id=xxx
Router.delete("/_token/delete", async (req, res) => {
  let id = req.query.id
  const delete_sql = "DELETE FROM `blog`  WHERE `id`= ?"//修改的sql语句
  let { err, rows } = await db.async.run(delete_sql, [id])
  if (err == null) {
    res.send({
      code: 200,
      msg: '删除成功'
    })
  } else {
    res.send({
      code: 500,
      msg: '删除失败'
    })
  }
})


//查询单篇blog的接口
Router.get("/detail", async (req, res) => {
  let { id } = req.query
  let detail_sql = "SELECT * FROM `blog` WHERE `id` = ? "
  let { err, rows } = await db.async.all(detail_sql, [id])

  if (err == null) {
    res.send({
      code: 200,
      msg: "查询成功",
      rows
    })
  } else {
    res.send({
      code: 500,
      msg: "获取失败"
    })
  }

})

//查询博客
Router.get("/search", async (req, res) => {
  /* 
  keyword 关键字
  categoryId 分类编号
  分页：
  page 页码
  pageSize 分页大小
  */
  let { keyword, categoryId, page, pageSize } = req.query
  //判断前端的传值
  page = page == null ? 1 : page //判断分页是否存在，不存在则取默认值 1
  pageSize = pageSize == null ? 10 : pageSize //分页大小
  categoryId = categoryId == null ? 0 : categoryId
  keyword = keyword == null ? "" : keyword

  let params = []//参数
  let whereSqls = []

  //判断分类编号是否存在
  if (categoryId != 0) {
    whereSqls.push(" `category_id` = ? ") //不存在就添加
    params.push(categoryId) //添加到参数
  }

  if (keyword != "") {//判断关键字查询是否存在
    whereSqls.push(" (`title` LIKE ? OR `content` LIKE ?) ") //模糊查询
    params.push("%" + keyword + "%") //添加到参数
    params.push("%" + keyword + "%") //添加到参数
  }

  let whereSqlStr = ""
  if (whereSqls.length > 0) { //当whereSql的长度 大于0 表示可能存在 分类编号 或 关键字  然后进行拼接sql语句
    whereSqlStr = " WHERE " + whereSqls.join(" AND ")
  }

  //将参数进行整合 查分页数据
  let searchSql = " SELECT `id`,`category_id`,`create_time`,`title`,substr(`content`,0,50) AS `content` FROM `blog` " + whereSqlStr + " ORDER BY `create_time` DESC LIMIT ?,? "
  let searchSqlParams = params.concat([(page - 1) * pageSize, pageSize])//分页处理
  console.log(searchSql, searchSqlParams)

  //查询数据总数
  let searchCountSql = " SELECT count(*) AS `count` FROM `blog` " + whereSqlStr//直接查询数据总数
  let searchCountParams = params
  console.log(searchCountSql)

  //分页数据
  let searchResult = await db.async.all(searchSql, searchSqlParams)
  let countResult = await db.async.all(searchCountSql, searchCountParams)

  //console.log(countResult, searchResult)

  if (searchResult.err == null && countResult.err == null) {
    res.send({
      code: 200,
      msg: '查询成功',
      data: {
        keyword,
        categoryId,
        page,
        pageSize,
        rows: searchResult.rows,
        count: countResult.rows[0].count
      }
    })
  } else {
    res.send({
      code: 500,
      msg: '查询失败'
    })
  }
})


//导出路由
module.exports = Router