const express = require("express")
const Router = express.Router()
const { db, genid } = require("../db/DBUtils")


/* //测试路由
Router.get("/test", (req, res) => {
  db.all("select * from`admin`", [], (err, rows) => {
    console.log(rows)
  })
  res.send({
    test: "测试路由",
    id: genid.NextId(),
  })
}) */

//测试路由
Router.get("/test", async (req, res) => {
  // db.all("select * from`admin`", [], (err, rows) => {
  //   console.log(rows)
  // })

  // db.async.all("select * from`admin`", []).then((res) => {
  //   console.log(res)
  // })

  let out = await db.async.all("select * from`admin`", [])

  res.send({
    test: "测试路由",
    id: genid.NextId(),
    out //对象的简易书写   out:out
  })
})



//导出路由
module.exports = Router