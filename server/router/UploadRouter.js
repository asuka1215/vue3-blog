const express = require("express")
const Router = express.Router()
const fs = require("fs")
const { db, genid } = require("../db/DBUtils")


//上传文件路由
Router.post("/rich_editor_upload", async (req, res) => {//针对富文本编辑器 wangEditor 的接口

  if (!req.files) {//如果没有上传文件
    res.send({
      "errno": 1, // 只要不等于 0 就行
      "message": "失败信息"
    })
    return
  }
  let files = req.files
  let ret_files = []//创建一个数组，用于保存文件名
  for (let file of files) {
    //遍历文件内容，获取文件名字后缀   例如 aa.jpg
    let file_ext = file.originalname.substring(file.originalname.lastIndexOf(".") + 1)
    //随机文件名字
    let file_name = genid.NextId() + "." + file_ext //用雪花算法随机名字加上文件后缀

    //修改文件名字  移动文件
    fs.renameSync(//fs.renameSync()方法用于将给定旧路径下的文件同步重命名为给定新路径。如果目标文件已经存在，它将覆盖目标文件。
      process.cwd() + "/public/upload/temp/" + file.filename,
      process.cwd() + "/public/upload/" + file_name
    )
    ret_files.push("/upload/" + file_name)//保存文件名
  }
  res.send({
    "errno": 0, // 注意：值是数字，不能是字符串
    "data": {
      "url": ret_files[0], // 图片 src ，必须
    }
  })
})



//导出路由
module.exports = Router