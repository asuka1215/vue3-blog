//multer 上传文件功能的中间件
//sqlite3  数据库，和mysql的区别时连接的方式
//uuid  用来生成唯一的标志


const express = require('express')
const multer = require('multer')
const path = require("path")
const app = express()
const { db, genid } = require("./db/DBUtils")

//配置静态资源文件
app.use(express.static(path.join(__dirname, "public")))

//开放跨域请求的处理
app.use(function (req, res, next) {
  //设置允许跨域的域名，* 代表允许任意域名
  res.header("Access-Control-Allow-Origin", "*")
  //允许的header类型
  res.header("Access-Control-Allow-Headers", "*")
  //允许跨域的请求方式
  res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS")
  if (req.method == "OPTIONS") res.sendStatus(200)//让options尝试请求快速结果
  else next()
})

//express.json() 是 Express 从 v4.16.0 开始的内置中间件功能。 它解析传入的 JSON 请求并将解析的数据放入 req.body。
app.use(express.json())

//dest 指定存储文件的本地位置 single 参数的名称 multer({dest:paths.join(__dirname,'../public/algorithm_file')}).single('file')
const update = multer({//上传文件的处理
  dest: "./public/upload/temp" //文件的路径
})
app.use(update.any())


//token验证的中间件
// 例如：category/_token/add  只要接口路径中含有 /_token 这个标记 都会进行token校验
const ADMIN_TOKEN_PATH = "/_token"
app.all("*", async (req, res, next) => {//所有的接口都先经过这个中间件才能往下执行
  if (req.path.indexOf(ADMIN_TOKEN_PATH) > -1) {//如果接口地址中包含 /_token
    let { token } = req.headers
    //console.log(token) //token是唯一的，如果传过来的token和admin表中匹配，那么就证明已经登录
    let admin_token_sql = " SELECT * FROM `admin` WHERE `token` = ? " //在admin表中查询token
    //查询结果
    let adminResult = await db.async.all(admin_token_sql, [token]) // [token]为参数，即传入的token值在admin表中进行匹配
    if (adminResult.err != null || adminResult.rows.length == 0) {
      //如果查询的结果 错误不等于空 或者 长度等于0，说明用户没有登录
      res.send({
        code: 403,
        msg: '请先登录'
      })
      return
    } else {
      next()
    }
  } else {//否则进行下一步
    next()
  }
})



//注册路由
let test = require("./router/TestRouter")
app.use('/test', test)

//注册admin路由
let admin = require("./router/AdminRouter")
app.use('/admin', admin)

//注册分类操作的路由
let category = require('./router/CategoryRouter')
app.use('/category', category)

//注册blog操作的路由
let blog = require('./router/BlogRouter')
app.use('/blog', blog)

//注册文件上传的路由
let upload = require('./router/UploadRouter')
app.use('/upload', upload)


app.listen(7766, () => {
  console.log('Asuka监听的端口7766')
})
